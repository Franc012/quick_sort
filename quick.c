#include <stdio.h>

void trocar(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int particionar(int array[], int baixo, int alto) {
    int pivo = array[alto]; 
    int i = (baixo - 1); 

    for (int j = baixo; j < alto; j++) {
        if (array[j] <= pivo) {
            i++; 
            trocar(&array[i], &array[j]); 
        }
    }
    trocar(&array[i + 1], &array[alto]); 
    return (i + 1); 
}

void quickSort(int array[], int baixo, int alto) {
    if (baixo < alto) {
        int pi = particionar(array, baixo, alto); 
        quickSort(array, baixo, pi - 1); 
        quickSort(array, pi + 1, alto); 
    }
}

void imprimirArray(int array[], int tamanho) {
    for (int i = 0; i < tamanho; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main() {
    int dados[] = {8, 7, 2, 1, 0, 9, 6};
    int n = sizeof(dados) / sizeof(dados[0]);

    printf("Array original: \n");
    imprimirArray(dados, n);

    quickSort(dados, 0, n - 1);

    printf("Array ordenado: \n");
    imprimirArray(dados, n);
    return 0;
}